//
//  LoginViewControllerr.swift
//  PCCSTask
//
//  Created by Jahnavi on 01/12/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var passwordTextFiled: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func proceed() {
        self.navigationController?.pushViewController(HomeScreenViewController.viewController(), animated: true)
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        if passwordTextFiled.text?.isEmpty == true {
            AlertViewController.presentAlertViewWithTitle("", message: "Please Enter Password")
        } else if passwordTextFiled.text?.trimmingCharacters(in: .whitespaces) == "12345678" {
            proceed()
        } else {
            AlertViewController.presentAlertViewWithTitle("", message: "Incorrect Password. Please try again.")
        }
    }
    
    
    @IBAction func screenTapped(_ sender: UITapGestureRecognizer) {
        
    }
    
}

