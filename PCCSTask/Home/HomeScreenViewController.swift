//
//  HomeScreenViewController.swift
//  PCCSTask
//
//  Created by Jahnavi on 01/12/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit

class HomeScreenViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var homeScreenCollectionView: UICollectionView!
    
    @IBOutlet weak var logoImageView: UIImageView!
    
    
    class func viewController() -> HomeScreenViewController {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController =  mainStoryboard.instantiateViewController(withIdentifier: "HomeScreenViewController") as! HomeScreenViewController
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home"
        self.navigationItem.setHidesBackButton(true, animated:true);
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        switch indexPath.row {
        case 0:
            cell.nameLabel.text = "Appointments"
        case 1:
            cell.nameLabel.text = "Engineer ID"
        case 2:
            cell.nameLabel.text = "Call Base"
        case 3:
            cell.nameLabel.text = "Find A Buddy"
        case 4:
            cell.nameLabel.text = "Start Of Day Routine"
        case 5:
            cell.nameLabel.text = "Settings"
        default:
            cell.nameLabel.text = ""
        }
        addGradient(cell: cell)
        return cell
    }
    
    func addGradient(cell: HomeCollectionViewCell) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [UIColor.darkGray.cgColor, UIColor.white.cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        cell.layer.insertSublayer(gradient, at: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.navigationController?.pushViewController(AppointmentsViewController.viewController(), animated: true)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width/4.0, height: collectionView.bounds.size.height/2.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
