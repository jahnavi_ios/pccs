//
//  HomeCollectionViewCell.swift
//  PCCSTask
//
//  Created by Jahnavi on 01/12/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
}
