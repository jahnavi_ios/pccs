//
//  DetailsNameTableViewCell.swift
//  PCCSTask
//
//  Created by Jahnavi on 02/12/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit

class DetailsNameTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    var delegate: DetailsViewControllerDelegate!
    
    let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"
    
    var valuesDict = [String: String]()

    
    func configureCell(delegate: DetailsViewControllerDelegate) {
        self.delegate = delegate
        if nameTextField.tag == 0 {
            valuesDict["name"] = nameTextField.text
        } else if nameTextField.tag == 1 {
            valuesDict["address"] = nameTextField.text
        } else if nameTextField.tag == 2 {
            valuesDict["poasCode"] = nameTextField.text
        }else if nameTextField.tag == 3 {
            valuesDict["phoneNumber"] = nameTextField.text
        }else if nameTextField.tag == 4 {
            valuesDict["manufacturer"] = nameTextField.text
        }else if nameTextField.tag == 5 {
            valuesDict["model"] = nameTextField.text
        }else if nameTextField.tag == 6 {
            valuesDict["serialNumber"] = nameTextField.text
        }else if nameTextField.tag == 7 {
            valuesDict["reportedFault"] = nameTextField.text
        }else if nameTextField.tag == 8 {
            valuesDict["serviceReport"] = nameTextField.text
        }else if nameTextField.tag == 9 {
            valuesDict["location"] = nameTextField.text
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if nameTextField.tag == 0 {
            nameTextField.keyboardType = UIKeyboardType.asciiCapable
        } else if nameTextField.tag == 1 {
            nameTextField.keyboardType = UIKeyboardType.asciiCapable
        } else if nameTextField.tag == 2 {
            nameTextField.keyboardType = UIKeyboardType.asciiCapable
        }else if nameTextField.tag == 3 {
             nameTextField.keyboardType = UIKeyboardType.numberPad
        }else if nameTextField.tag == 4 {
            
        }else if nameTextField.tag == 5 {
             //moveTextField(textField, moveDistance: -250, up: true)
        }else if nameTextField.tag == 6 {
             nameTextField.keyboardType = UIKeyboardType.numberPad
           //  moveTextField(textField, moveDistance: -250, up: true)
        }else if nameTextField.tag == 7 {
           //  moveTextField(textField, moveDistance: -250, up: true)
        }else if nameTextField.tag == 8 {
             nameTextField.keyboardType = UIKeyboardType.numberPad
            // moveTextField(textField, moveDistance: -250, up: true)
        }else if nameTextField.tag == 9 {
             //moveTextField(textField, moveDistance: -250, up: true)
        }
       
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if nameTextField.tag == 0 {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        } else if nameTextField.tag == 1 {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        } else if nameTextField.tag == 2 {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }else {
            return true
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        if nameTextField.tag == 0 {
            valuesDict["name"] = nameTextField.text
        } else if nameTextField.tag == 1 {
             valuesDict["address"] = nameTextField.text
        } else if nameTextField.tag == 2 {
             valuesDict["poasCode"] = nameTextField.text
        }else if nameTextField.tag == 3 {
             valuesDict["phoneNumber"] = nameTextField.text
        }else if nameTextField.tag == 4 {
             valuesDict["manufacturer"] = nameTextField.text
        }else if nameTextField.tag == 5 {
            valuesDict["model"] = nameTextField.text
        }else if nameTextField.tag == 6 {
          valuesDict["serialNumber"] = nameTextField.text
        }else if nameTextField.tag == 7 {
            valuesDict["reportedFault"] = nameTextField.text
        }else if nameTextField.tag == 8 {
           valuesDict["serviceReport"] = nameTextField.text
        }else if nameTextField.tag == 9 {
           valuesDict["location"] = nameTextField.text
        }
        delegate.getAllValues(values: valuesDict)
    }
    
    
    
    
}
