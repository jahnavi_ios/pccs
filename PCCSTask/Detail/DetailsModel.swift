//
//  DetailsModel.swift
//  PCCSTask
//
//  Created by Jahnavi on 02/12/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit

class DetailsModel: NSObject {
   
    var customerName = ["Mr Anikumar Discon", "Mrs Crabtree", "Mr Tony Plant"]
    var customerAddress = ["10-11, Henson Close, Ketting", "Ryehill Farm, Long Buckby Wharf, Northants" , "3 Temple Close, Moreton Grange, Buckingham, Bucks"]
    var customerPostCode = ["NN16 8PZ", "NN6 7PW", "MK18 1JB"]
    var customerPhoneNumber = ["07725748131", "07725748131", "07725748131"]
    var manufacturer = ["Samsung", "", "Toshiba"]
    var model = ["", "LE145665", ""]
    var serialNumber = ["1212511221", "", ""]
    var reportedFault = ["The TV is damaged with a crack across the screen.", "No sound played from unit, was dropped from table.", "Does not connect to internet."]
    var serviceRaport = ["", "", ""]
     var currentLocation = ["", "", ""]
    
}
