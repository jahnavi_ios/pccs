//
//  ManufacturerHandler.swift
//  PCCSTask
//
//  Created by Jahnavi on 02/12/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit

protocol ManufacturerHandlerDelegate {
    func selectedManufacturer(manufacturer: String)
}


class ManufacturerHandler: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    
    private var textField : UITextField!
    var pickerView: UIPickerView!
    var delegate: ManufacturerHandlerDelegate!
    var width: CGFloat!
    var arrayOfStrings = ["Alba","Hitachi","Samsung","Sony","Toshiba"]
    
    required init(pickerView: UIPickerView, width: CGFloat) {
        self.pickerView = pickerView
        self.width = width
        super.init()
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayOfStrings.count
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return width
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        delegate.selectedManufacturer(manufacturer: arrayOfStrings[row])
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayOfStrings[row]
    }
    
    
}


