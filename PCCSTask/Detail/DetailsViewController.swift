//
//  DetailsViewController.swift
//  PCCSTask
//
//  Created by Jahnavi on 02/12/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit
import MapKit
import MessageUI

protocol DetailsViewControllerDelegate {
    func getAllValues(values: Dictionary<String, String>)
}

class DetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, ManufacturerHandlerDelegate, ModelHandlerDelegate, MFMailComposeViewControllerDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, DetailsViewControllerDelegate {
    
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var modelPickerView: UIPickerView!
    @IBOutlet weak var detailsTableView: UITableView!
    @IBOutlet weak var deatilImageView: UIImageView!
    var valuesDict = [String: String]()
    private var manufacturerPickerHandler: ManufacturerHandler!
    private var modelPickerHandler: ModelHandler!
    var manufacturer = String()
    var pickedModel = String()
    var isFieldEmpty: Bool = false
    var indePath: Int!
    
    class func viewController(indePath: Int) -> DetailsViewController {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController =  mainStoryboard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        viewController.indePath = indePath
        return viewController
    }
    
    var lat  = CLLocationDegrees()
    var long = CLLocationDegrees()
    
    var locationManager = CLLocationManager()
    var model = DetailsModel()
    var data = [String]()
    var dataForFirstRow = [String]()
    var dataForSecondRow = [String]()
    var dataForThridRow = [String]()
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        data = ["Enter Customer Name", "Enter Customer Addres", "Enter Customer PostCode", "Enter Customer Phone Number", "Enter Manufacturer", "Enter Model", "Enter Serial Number","Enter Reported Fault", "Enter Service Report", "Your Location"]
        dataForFirstRow = ["Mr Anikumar Discon", "10-11, Henson Close, Ketting","NN16 8PZ", "07725748131", "Samsung" , "", "1212511221", "The TV is damaged with a crack across the screen.", "", "latitude: \(lat),longitude\(long)"]
        dataForSecondRow = ["Mrs Crabtree", "Ryehill Farm, Long Buckby Wharf, Northants", "NN6 7PW", "07725748131", "", "LE145665", "", "No sound played from unit, was dropped from table.", "", "latitude: \(lat),longitude\(long)"]
        dataForThridRow = ["Mr Tony Plant","3 Temple Close, Moreton Grange, Buckingham, Bucks", "MK18 1JB", "07725748131", "Toshiba", "", "", "Does not connect to internet.", "", "latitude: \(lat),longitude\(long)"]
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        lat = locValue.latitude
        long = locValue.longitude
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsNameTableViewCell", for: indexPath) as! DetailsNameTableViewCell
       
        cell.nameLabel.text = data[indexPath.row]
        cell.nameTextField.tag = indexPath.row
        cell.nameTextField.delegate = cell
        if indePath == 0 {
            cell.nameTextField.text = dataForFirstRow[indexPath.row]
        } else if indePath == 1 {
            cell.nameTextField.text = dataForSecondRow[indexPath.row]
        } else {
            cell.nameTextField.text = dataForSecondRow[indexPath.row]
        }
        if indexPath.row == 4 {
            cell.nameTextField.inputView             = pickerView
            manufacturerPickerHandler = ManufacturerHandler(pickerView: pickerView, width: self.view.frame.width)
            manufacturerPickerHandler.delegate = self
            if manufacturer.isEmpty != true {
                cell.nameTextField.text = manufacturer
            }
        }
        if indexPath.row == 5 {
            cell.nameTextField.inputView             = modelPickerView
            modelPickerHandler = ModelHandler(pickerView: modelPickerView, width: self.view.frame.width)
            modelPickerHandler.delegate = self
            if pickedModel.isEmpty != true {
                cell.nameTextField.text = pickedModel
            }
        }
         cell.configureCell(delegate: self)
        return cell
    }
    
    func selectedManufacturer(manufacturer: String) {
        self.manufacturer = manufacturer
        detailsTableView.reloadData()
    }
    
    func selectedModel(model: String) {
        self.pickedModel = model
        detailsTableView.reloadData()
    }
    
    var subjectString: String!
    func getAllValues(values: Dictionary<String, String>) {
        valuesDict = values
        print(values.count)
        for (key, value) in values {
            print("\(key) -> \(value)")
            
            if value.isEmpty == true {
                isFieldEmpty = false
            } else {
                isFieldEmpty = true
            }
        }
    }
    
    
    @IBAction func completeAndEmailButtonTapped(_ sender: UIButton) {
        detailsTableView.reloadData()
        if self.isFieldEmpty == false {
            AlertViewController.presentAlertViewWithTitle("", message: "Please fill all the field")
        } else {
            let mailComposerVC = MFMailComposeViewController()
            mailComposerVC.mailComposeDelegate = self as? MFMailComposeViewControllerDelegate
            mailComposerVC.setToRecipients(["anjahnavi15994@gmail.com"])
            for (key, value) in valuesDict {
                print("\(key) -> \(value)")
                
            }
            // mailComposerVC.setSubject("\(Key): \()")
            mailComposerVC.setMessageBody("", isHTML: false)
            if MFMailComposeViewController.canSendMail() == true {
                
                
                
                self.present(mailComposerVC, animated: true, completion: nil)
            } else {
                AlertViewController.presentAlertViewWithTitle("", message: "Unable to open mail")
            }
        }
    }
    
    @IBAction func screenTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func uploadImageButtonTapped(_ sender: UIButton) {
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: "", preferredStyle:.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) { _ in
            if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self
                imagePickerController.sourceType = UIImagePickerControllerSourceType.camera
                imagePickerController.allowsEditing = true;
                self.present(imagePickerController, animated: true, completion: nil)
            }
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) { _ in
            if UIDevice.current.userInterfaceIdiom == .phone {
                let imagePickerController = UIImagePickerController()
                imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
                imagePickerController.delegate = self
                imagePickerController.allowsEditing = true
                self.present(imagePickerController, animated: true, completion: nil)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        if UIDevice.current.userInterfaceIdiom == .phone {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func runInMainQueue(_ closure: @escaping () -> Void) {
        DispatchQueue.main.async(execute: closure)
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        DispatchQueue.global(qos: .userInitiated).async {
            let profileImage = info[UIImagePickerControllerEditedImage] as! UIImage
            self.runInMainQueue() {
                self.deatilImageView.image = profileImage
            }
        }
        picker.dismiss(animated: false, completion: nil)
    }
    
    
}
