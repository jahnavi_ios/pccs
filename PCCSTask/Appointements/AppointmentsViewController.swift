//
//  AppointmentsViewController.swift
//  PCCSTask
//
//  Created by Jahnavi on 01/12/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit

class AppointmentsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var appointmentsTableView: UITableView!
    
    class func viewController() -> AppointmentsViewController {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController =  mainStoryboard.instantiateViewController(withIdentifier: "AppointmentsViewController") as! AppointmentsViewController
        return viewController
    }
    
    var model = AppointementsModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
        label.textColor = .white
        label.text = "Today's Appointments\n\(getDate()!)"
        self.navigationItem.titleView = label
    }
    
    func getDate() -> String? {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "EE/dd/MM/yyyy"
        let result = formatter.string(from: date)
        return result
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentsTableViewCell", for: indexPath) as! AppointmentsTableViewCell
        cell.addressLabel.text = model.nolabelCustomerAddress[indexPath.row]
        cell.travelLabel.text = model.travelArray[indexPath.row]
        cell.arriveLabel.text = model.arriveArray[indexPath.row]
        cell.durationLabel.text = model.duration[indexPath.row]
        cell.appSlotLabel.text = model.slot[indexPath.row]
        cell.appWindowLabel.text = model.window[indexPath.row]
        cell.appointementType.text = model.appointmentType[indexPath.row]
        cell.clinetLabel.text = model.client[indexPath.row]
        cell.branchLabel.text = model.branceh[indexPath.row]
        cell.nameLabel.text = model.noLabelCustomerName[indexPath.row]
        cell.addressLabel.text = model.nolabelCustomerAddress[indexPath.row]
        cell.phoneNumber.setTitle(model.noLabelCustomerPhoneNumber[indexPath.row], for: .normal)
        cell.postCardButton.setTitle(model.noLabelCustomerPhonePostcode[indexPath.row], for: .normal)
        cell.serialNumberLabel.text = model.serialNumber[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(DetailsViewController.viewController(indePath: indexPath.row), animated: true)
    }
    
    @IBAction func phoneNumberButtonTapped(_ sender: UIButton) {
        guard let cell = sender.superview?.superview?.superview as? AppointmentsTableViewCell else { return }
        print(cell)
        if let cell = sender.superview?.superview?.superview as? AppointmentsTableViewCell {
            let indexPathForCell = self.appointmentsTableView.indexPath(for: cell)
            let indexpath = IndexPath(row: indexPathForCell!.row, section: 0)
            if let url = URL(string: "\(model.noLabelCustomerPhoneNumber[indexpath.row])") {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
    }
    
    
    @IBAction func navigateToLocationButtonTapped(_ sender: UIButton) {
       
    }
    


}
