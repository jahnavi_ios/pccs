
//
//  AppointmentsTableViewCell.swift
//  PCCSTask
//
//  Created by Jahnavi on 02/12/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit

class AppointmentsTableViewCell: UITableViewCell {

    @IBOutlet weak var serialNumberLabel: UILabel!
    @IBOutlet weak var travelLabel: UILabel!
    
    @IBOutlet weak var arriveLabel: UILabel!
    
    @IBOutlet weak var durationLabel: UILabel!
    
    @IBOutlet weak var appSlotLabel: UILabel!
    
    @IBOutlet weak var appWindowLabel: UILabel!
    
    @IBOutlet weak var appointementType: UILabel!
    
    @IBOutlet weak var clinetLabel: UILabel!
    
    @IBOutlet weak var branchLabel: UILabel!
    
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var phoneNumber: UIButton!
    
    @IBOutlet weak var postCardButton: UIButton!
    
}
