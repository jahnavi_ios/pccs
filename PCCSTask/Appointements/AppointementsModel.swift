//
//  AppointementsModel.swift
//  PCCSTask
//
//  Created by Jahnavi on 02/12/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit

class AppointementsModel: NSObject {
    
    
    var travelArray = ["00.58", "00.19", "01:39"]
    var arriveArray = ["09:30", "10:45", "12:56"]
    var duration = ["00:25", "00:25", "00:35"]
    var SL = ["88554455", "88554459", "88554896"]
    var slot = ["ANY(09:30 - 17:00)", "AM(09:30 - 12:00)", "ANY(09:30 - 17:00)"]
    var window = ["(09:30 - 17:00)", "(09:30 - 12:00)", "(09:30 - 17:00)"]
    var appointmentType = ["Repair", "Delivery", "Collection"]
    var client = ["INTEGRATED SERVICES (NORTHANTS) LTD (Non-trade Account ISL)", "INTEGRATED SERVICES (NORTHANTS) LTD (Non-trade Account ISL)", "ARGOS"]
    var branceh = ["ISL Workshop", "ISL Workshop", "Daventry Branch NN7 4PO"]
    var noLabelCustomerName = ["Mr Anikumar Discon", "Mrs Crabtree", "Mr Tony Plant"]
    var nolabelCustomerAddress = ["10-11, Henson Close, Ketting", "Ryehill Farm, Long Buckby Wharf, Northants", "3 Temple Close, Moreton Grange, Buckingham, Bucks"]
    var noLabelCustomerPhoneNumber = ["07725748131", "07725748131", "07725748131"]
    var noLabelCustomerPhonePostcode = [" NN16 8PZ", "NN6 7PW", "MK18 1JB"]
    var serialNumber = ["1", "2", "3"]

}
