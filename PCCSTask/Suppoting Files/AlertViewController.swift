//
//  AlertViewController.swift
//  PCCSTask
//
//  Created by Jahnavi on 01/12/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import Foundation
import UIKit

class AlertViewController {
    
    class func presentAlertViewWithTitle(_ title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
        alertController.addAction(alertAction)
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let currentViewController = appDelegate?.window?.rootViewController {
            currentViewController.present(alertController, animated: true, completion: nil)
        }
}

}

